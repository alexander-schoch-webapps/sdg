import brentsMethod from "brents-method";

const antoineParams = {
  Ethanol: [5.24677, 1598.673, -46.424],
  Toluene: [4.07827, 1343.943, -53.773],
  EthylAcetate: [4.22809, 1245.702, -55.189],
  Benzene: [4.72583, 1660.652, -1.461],
  Methanol: [5.31301, 1676.569, -21.728],
  Cyclohexane: [4.13983, 1316.554, -35.581],
  Isopropanol: [4.861, 1357.427, -75.814],
  Acetone: [4.42448, 1312.253, -32.445],
  "n-Hexane": [4.00266, 1171.53, -48.784],
  "Diethyl Ether": [4.46988, 1354.913, -5.537],
  Cyclohexene: [3.9973, 1221.9, -49.98],
  "1-Propanol": [4.87601, 1441.629, -74.299],
  "n-Heptane": [4.02832, 1268.636, -56.199],
  Tetrahydrofuran: [4.12118, 1202.942, -46.818],
  Acetonitrile: [4.27873, 1355.374, -37.853],
  "1-Butanol": [4.54607, 1351.555, -93.34],
  Methylcyclohexane: [4.126, 1375.13, -40.331],
};

export function getAntoine(compound) {
  return antoineParams[compound];
}

export function antoine(T, c) {
  return Math.pow(10, c[0] - c[1] / (T + 273.15 + c[2]));
}

export function wilson1(x1, L12, L21) {
  let x2 = 1 - x1;
  let lng =
    -Math.log(x1 + L12 * x2) +
    x2 * (L12 / (x1 + L12 * x2) - L21 / (L21 * x1 + x2));
  return Math.exp(lng);
}

export function wilson2(x2, L12, L21) {
  let x1 = 1 - x2;
  let lng =
    -Math.log(x2 + L12 * x1) -
    x1 * (L12 / (x1 + L12 * x2) - L21 / (x2 + L21 * x1));
  return Math.exp(lng);
}

export function raoult1(x1, PV, L12, L21, P) {
  return (x1 * wilson1(x1, L12, L21) * PV) / P;
}

export function raoult2(x2, PV, L12, L21, P) {
  return (x2 * wilson2(x2, L21, L12) * PV) / P;
}

export function rootfinding(T, xi, L12, L21, C1, C2, P) {
  return (
    raoult1(xi, antoine(T, getAntoine(C1)), L12, L21, P) +
    raoult2(1 - xi, antoine(T, getAntoine(C2)), L12, L21, P) -
    1
  );
}

export function get_T(x, L12, L21, C1, C2, P) {
  const T = brentsMethod(
    (T) => rootfinding(T, x, L12, L21, C1, C2, P),
    0,
    150,
    { maxIterations: 100 },
  );
  return T;
}

export function get_xg(x, L12, L21, T, C1, P) {
  let PV = antoine(T, getAntoine(C1));
  return raoult1(x, PV, L12, L21, P);
}
