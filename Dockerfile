FROM node:20 AS build

# Create app directory
RUN mkdir -p /sdg
WORKDIR /sdg

# Install app dependencies
COPY package.json .
RUN npm i

# Bundle app source
COPY . .

# Build and optimize react app
RUN npm run build
#RUN npm i -g serve


FROM nginx:alpine as run

COPY --from=build /sdg/build /usr/share/nginx/html